package me.terturl.com;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.libs.jline.internal.Log;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin
{
  Logger log = Logger.getLogger("Minecraft");
  public static Permission perm = null;
  public static Economy econ = null;
  public static Chat chat = null;
  
  private boolean setupPermissions()
  {
    RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(Permission.class);
    if (permissionProvider != null) {
      perm = (Permission)permissionProvider.getProvider();
    }
    return perm != null;
  }

  private boolean setupChat()
  {
    RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(Chat.class);
    if (chatProvider != null) {
      chat = (Chat)chatProvider.getProvider();
    }

    return chat != null;
  }

  private boolean setupEconomy()
  {
    RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(Economy.class);
    if (economyProvider != null) {
      econ = (Economy)economyProvider.getProvider();
    }

    return econ != null;
  }
  
  public void log(Log.Level level, String message) {
	    message = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " [" + level.name() + "] " + message;
	    try {
	      File dataFolder = getDataFolder();
	      if (!dataFolder.exists()) {
	        dataFolder.mkdir();
	      }
	      File saveTo = new File(getDataFolder(), "log.txt");
	      if (!saveTo.exists()) {
	        saveTo.createNewFile();
	      }
	      FileWriter fw = new FileWriter(saveTo, true);
	      PrintWriter pw = new PrintWriter(fw);
	      pw.println(message);
	      pw.flush();
	      pw.close();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	  }
  public void log(String message) { log(Log.Level.INFO, message); }

  public void onEnable() {
	  
    this.log.info(String.format("[%s] Enabled Version %s", new Object[] { getDescription().getName(), getDescription().getVersion() }));
    this.log.log(Level.FINE, "Please report all bugs on the Bukkit page!");

    setupChat();
    setupPermissions();
    setupEconomy();

    if (!setupChat()) {
      this.log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", new Object[] { getDescription().getName() }));
      getServer().getPluginManager().disablePlugin(this);
      return;
    }
    if (!setupEconomy()) {
      this.log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", new Object[] { getDescription().getName() }));
      getServer().getPluginManager().disablePlugin(this);
      return;
    }
    if (!setupPermissions()) {
      this.log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", new Object[] { getDescription().getName() }));
      getServer().getPluginManager().disablePlugin(this);
      return;
    }
  }

  public void onDisable() {
    this.log.info(String.format("[%s] Disabled Version %s", new Object[] { getDescription().getName(), getDescription().getVersion() }));
  }

  public boolean onCommand(final CommandSender sender, Command command, String label, String[] args) {
    Player p = (Player)sender;

    if (label.equalsIgnoreCase("civ")) { 
        if (args.length == 0) {
      	 p.sendMessage(ChatColor.GREEN + "------Help------");
        	p.sendMessage(ChatColor.AQUA + "/research" + ChatColor.GREEN + " Displays research help");
        	p.sendMessage(ChatColor.AQUA + "/civ" + ChatColor.GREEN + " Displays civ help");
        	p.sendMessage(ChatColor.AQUA + "/build" + ChatColor.GREEN + " Displays building help");
        	p.sendMessage(ChatColor.AQUA + "/camp" + ChatColor.GREEN + " Displays campsite help");
        	p.sendMessage(ChatColor.GREEN + "----------------");
        }
        
        if(args.length == 1) {
      	if(args[0].equalsIgnoreCase("civ")) {
      		
      	}
      	
      	if(args[0].equalsIgnoreCase("research")) {
      		p.sendMessage(ChatColor.AQUA + "tier1 ~ Lists all tier 1 technology");
      		p.sendMessage(ChatColor.AQUA + "tier2 ~ Lists all tier 2 technology");
      		p.sendMessage(ChatColor.AQUA + "tier3 ~ Lists all tier 3 technology");
   		
      		if (args[1].equalsIgnoreCase("agriculture")) {
    	          if (perm.has(p, "civ.tech.ancient.agriculture")) {
    	            p.sendMessage(ChatColor.RED + "You have already researched tech!");
    	          }
    	          else if (econ.has(p.getName(), 5000.0D)) {
    	            p.sendMessage(ChatColor.GOLD + "This tech will take 30 minutes to research");
    	            econ.withdrawPlayer(p.getName(), 5000.0D);
    	            Bukkit.getScheduler().runTaskLater(this, new Runnable()
    	            {
    	              public void run() {
    	                Player p = (Player)sender;
    	                p.sendMessage(ChatColor.GOLD + "You have finished researching agriculture!");
    	                perm.playerAdd(p, "civ.tech.ancient.agriculture");
    	              }
    	            }
    	            , 36000L); } else {
    	            p.sendMessage(ChatColor.RED + "You need at least $5000 to research this tech!");
    	          }
    	        }
      		}
        }
     }

    return false;
  }
  
}